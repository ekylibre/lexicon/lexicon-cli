# Lexicon::Cli

This repository provides a CLI to download/upload/load/enable compiled lexicon packages into a Postgres (PostGIS) database.

You can read the [documentation in the `doc/` folder](doc/usage.md)